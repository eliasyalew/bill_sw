/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sas;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author Elijah
 */
public class SAS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
        users x= new users();
        x.setVisible(true);
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Program Loading Error!", 0);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                final mainFrame mf = new mainFrame();
                mf.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        //mf.softExit();
                    }
                });
                mf.setTitle("SAS - Main page");
                mf.setVisible(true);

                //mf.security.setForeground(Color.BLUE);
                //mf.services.setEnabled(true);
                //mf.connection.setEnabled(true);
                //mf.conn.setVisible(true);
                //mf.disc.setVisible(true);
                //mf.clean_statistics.setVisible(true);
                //mf.message_text_statisctics.setVisible(true);
              
               
            }
        });
    }
    
}
