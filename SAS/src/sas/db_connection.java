/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Elijah
 */
public class db_connection {
    public boolean make_conn() throws ClassNotFoundException, SQLException{
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        return true;
    }
    public Connection return_con() throws SQLException{
        String con = "jdbc:sqlserver://localhost:1433;databaseName=GENERAL;user=sa;password=sa";
        Connection conn = DriverManager.getConnection(con);
        return conn;
    }
    public static void main(String[] args) throws SQLException{
        db_connection db =new db_connection();
        System.out.println(db.return_con());
    }
}
